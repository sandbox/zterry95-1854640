<?php
/**
 * Implement of hook_elements()
 */
function string_proceed_elements() {
  $types['combo'] = array(
    '#input' => TRUE,
    '#process' => array('combo_element_process'),
    '#element_validate' => array('dom_element_validate'),
    '#autocomplete_path' => FALSE,
    '#default_value' => array(
      'preg' => '',
      'delta' => 0,
      'type' => '',
    ),
  );
  return $types;
}
/**
 * callback of element process
 */
function combo_element_process($element, $edit, &$form_state, $complete_form) {
  $element['#tree'] = TRUE;
  $element['id']  = array(
    '#type' => 'markup',
    '#value'  =>  $element['#value']['id'],
  );
  $element['status'] = array(
    '#type' => 'checkbox',
    '#value' => $element['#value']['status'],
    '#default_value' => $element['#value']['status'],
    '#descripton'=> 'status',
  );
  $element['type'] =  array(
    '#type' => 'select',
    '#value' => $element['#value']['type'],
    '#default_value' => $element['#value']['type'],
    '#options'=> array('replace'=>'replace','delete'=>'delete','preg'=>'preg','innertext'=>'innertext'),
  );
  $element['orig'] =  array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 1024,
    '#required' => TRUE,
    '#value' => $element['#value']['orig'],
  );
  $element['pcd'] =  array(
    '#type' => 'textfield',
    '#value' => $element['#value']['pcd'],
    '#size' => 50,
    '#maxlength' => 1024,
    '#default_value' => $element['#value']['pcd'],
  );

  return $element;
}
/**
 * Implements of hook_theme
 */
function string_proceed_theme() {
  return array(
    'combo' => array(
      'arguments' => array('element'),
      'file' => 'combo.inc',
    ),
  );
}
/**
 * theme for combo element.
 */
function theme_combo($element) {
  return theme('form_element', $element, '<div class="container-inline">' . $element['#children'] . '</div>');
}

